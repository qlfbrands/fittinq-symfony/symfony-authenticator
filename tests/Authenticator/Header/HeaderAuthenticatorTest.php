<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\Authenticator\Header;

use Fittinq\Symfony\Authenticator\Authenticator\HeaderAuthenticator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class BearerAuthenticatorTest extends TestCase
{
    private Configuration $configuration;
    private HeaderAuthenticator $authenticator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->authenticator = $this->configuration->configure();
    }

    public function test_throwAuthenticationExceptionWhenAuthorizationIsNotSet()
    {
        $request = new Request();

        $this->assertFalse($this->authenticator->supports($request));
    }

    public function test_supportSuccessWhenAuthorizationIsSet()
    {
        $request = $this->configuration->createRequestWithJwt("Bearer invalid_token");

        $this->assertTrue($this->authenticator->supports($request));
    }

    public function test_throwAuthenticationExceptionWhenCookieIsMissing()
    {
        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate(new Request());
    }

    public function test_throwAuthenticationExceptionWhenAuthorizationIsMissingBearer()
    {
        $request = $this->configuration->createRequestWithJwt("eya0sdfu0a9sdf09.a0sdfua0sdf.as0dfjasdif");

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenAuthorizationIsInvalid()
    {
        $request = $this->configuration->createRequestWithJwt("Bearer invalid_token");

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenUsernameIsMissing()
    {
        $request = $this->configuration->createRequest(null, ["ROLE_USER"], time() + 1000);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenRolesAreMissing()
    {
        $request = $this->configuration->createRequest('frank', null, time() + 1000);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenBearerJWTHasExpired()
    {
        $request = $this->configuration->createRequest("peter", ["ROLE_USER"], 1000);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_userSuccessfullyAuthenticatesByBearerToken()
    {
        $username = 'frank';
        $roles =  ["ROLE_USER"];
        $request = $this->configuration->createRequest($username, $roles, time() + 1000);

        $passport = $this->authenticator->authenticate($request);
        $user = $passport->getUser();

        $this->assertEquals($username, $user->getUsername());
        $this->assertEquals($roles, $user->getRoles());
    }

    public function test_onSuccessDoNotDoAnything()
    {
        $this->assertNull($this->authenticator->onAuthenticationSuccess(
            new Request(),
            $this->createMock(TokenInterface::class),
            'firewall'
        ));
    }
}