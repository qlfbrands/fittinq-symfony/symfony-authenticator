<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\Authenticator\Cookie;

use Firebase\JWT\JWT;
use Fittinq\Symfony\Authenticator\Authenticator\CookieAuthenticator;
use Fittinq\Symfony\Authenticator\Authenticator\JWTUserProvider;
use stdClass;
use Symfony\Component\HttpFoundation\Request;

class Configuration
{
    private ?CookieAuthenticator $authenticator = null;

    public function configure(string $authenticatorURL): CookieAuthenticator
    {
        if (!$this->authenticator) {
            $this->authenticator = new CookieAuthenticator(
                $authenticatorURL,
                new JWTUserProvider("tests/jwt.key.pub")
            );
        }

        return $this->authenticator;
    }

    public function createRequest(?string $username = null, ?array $roles = null, ?int $expiresAt = null): Request
    {
        $jwt = JWT::encode(
            $this->getPayload($username, $roles, $expiresAt),
            file_get_contents("tests/jwt.key"),
            'RS256'
        );
        return new Request([], [], [], ['jwt' => $jwt]);
    }

    private function getPayload(?string $username, ?array $roles, ?int $expiresAt): array
    {
        $payload = [];

        if ($username) {
            $payload["username"] = $username;
        }
        if ($roles) {
            $payload["roles"] = $roles;
        }
        if ($roles) {
            $payload["expires_at"] = $expiresAt;
        }

        return $payload;
    }
}