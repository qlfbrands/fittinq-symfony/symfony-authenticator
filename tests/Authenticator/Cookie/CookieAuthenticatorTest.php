<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\Authenticator\Cookie;

use Fittinq\Symfony\Authenticator\Authenticator\CookieAuthenticator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CookieAuthenticatorTest extends TestCase
{
    private CookieAuthenticator $authenticator;
    private Configuration $configuration;
    private string $authenticatorURL;

    protected function setUp(): void
    {
        $this->configuration = new Configuration();
        $this->authenticatorURL = 'http://dev.auth.hip.fittinq.com';
        $this->authenticator = $this->configuration->configure($this->authenticatorURL);
    }

    public function test_throwAuthenticationExceptionWhenCookieIsMissing()
    {
        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate(new Request());
    }

    public function test_throwAuthenticationExceptionWhenJWTPayloadIsNull()
    {
        $request = new Request();

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenJWTPayloadIsInvalid()
    {
        $request = new Request([], [], [], ['jwt' => 'i_am_not_a_jwt']);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenUsernameIsMissing()
    {
        $request = $this->configuration->createRequest(null, ["ROLE_USER"], time() + 1000);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenRolesAreMissing()
    {
        $request = $this->configuration->createRequest('frank', null, time() + 1000);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_throwAuthenticationExceptionWhenJWTHasExpired()
    {
        $request = $this->configuration->createRequest("peter", ["ROLE_USER"], 1000);

        $this->expectException(AuthenticationException::class);

        $this->authenticator->authenticate($request);
    }

    public function test_userSuccessfullyAuthenticates()
    {
        $username = 'frank';
        $roles =  ["ROLE_USER"];
        $request = $this->configuration->createRequest($username, $roles, time() + 1000);

        $passport = $this->authenticator->authenticate($request);
        $user = $passport->getUser();

        $this->assertEquals($username, $user->getUsername());
        $this->assertEquals($roles, $user->getRoles());
    }

    public function test_onSuccessDoNotDoAnything()
    {
        $this->assertNull($this->authenticator->onAuthenticationSuccess(
            new Request(),
            $this->createMock(TokenInterface::class),
            'firewall'
        ));
    }

    public function test_redirectUserToAuthenticatorLoginPageOnAuthenticationFailure()
    {
        $uri = 'https://example.com/secured_area';
        $response = $this->authenticator->onAuthenticationFailure(
            Request::create($uri),
            new AuthenticationException()
        );

        $url = "{$this->authenticatorURL}/login?callback={$uri}";
        $this->assertEquals(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertEquals($url, $response->headers->get('location'));
    }
}
