<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\Controller;

use Fittinq\Symfony\Authenticator\Controller\CallbackController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class CallbackControllerTest extends TestCase
{
    private CallbackController $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $configuration = new Configuration();
        $this->controller = $configuration->configure();
    }

    public function test_requestSetsCookieWithJWTForAuthenticationInOtherSystems()
    {
        $jwt = 'valid_jwt_token';
        $request = Request::create("https://app.io/authenticator-callback?jwt={$jwt}");
        $response = $this->controller->callback($request);
        $this->assertEquals($jwt, $response->headers->getCookies()[0]->getValue());
    }

    /**
     * We test this because this callback isn't responsible for authentication. The CookieAuthenticator class is
     * responsible for this.
     */
    public function test_requestSetsCookieWithJWTForAuthenticationInOtherSystemsEvenIfJWTIsInvalid()
    {
        $jwt = 'invalid_jwt_token';
        $request = Request::create("https://app.io/authenticator-callback?jwt={$jwt}");
        $response = $this->controller->callback($request);
        $this->assertEquals($jwt, $response->headers->getCookies()[0]->getValue());
    }

    public function test_redirectTheUserToThePageThatWasInitiallyRequested()
    {
        $jwt = 'valid_jwt_token';
        $uri = 'https://app.io/users';
        $request = Request::create("https://app.io/authenticator-callback?jwt={$jwt}&uri={$uri}");
        $response = $this->controller->callback($request);

        $this->assertEquals($uri, $response->headers->get('location'));
    }

    public function test_redirectTheUserToTheRootIfUriHostDoesNotMatchRequestHost()
    {
        $jwt = 'valid_jwt_token';
        $uri = 'https://anotherapp.io/users';
        $request = Request::create("https://app.io/authenticator-callback?jwt={$jwt}&uri={$uri}");
        $response = $this->controller->callback($request);

        $this->assertEquals('https://app.io', $response->headers->get('location'));
    }

    public function test_redirectTheUserToTheRootIfNoPathWasGiven()
    {
        $jwt = 'valid_jwt_token';
        $request = Request::create("https://app.io/authenticator-callback?jwt={$jwt}");
        $response = $this->controller->callback($request);

        $this->assertEquals('https://app.io', $response->headers->get('location'));
    }
}
