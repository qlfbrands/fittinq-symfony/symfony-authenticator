# Symfony authenticator
Shield of your service with the authenticator library

## Install via composer
```bash
composer require fittinq\symfony-authenticator
```
## Configure bundle
```php
# config/bundles.php
<?php

return [
    // ...
    Fittinq\Symfony\Authenticator\SymfonyAuthenticatorBundle::class => ['all' => true],
];

```
#.env
Configure the following .env.local settings
```ini
AUTHENTICATOR_HOST_URL=dev.auth.hip.fittinq.com
AUTHENTICATOR_KEY_PUB=/var/www/app/auth.key.pub
```

# Setup security.yaml

```yaml
security:
  enable_authenticator_manager: true

  providers:
    user_provider:
      id: Fittinq\Symfony\Authenticator\Authenticator\JwtUserProvider

  firewalls:
    unsecure:
      pattern: ^/(health|authenticator-callback)
      security: false
    secure:
      provider: user_provider
      custom_authenticators:
#        Depending on your problem, you can add either authenticator.
#        - Fittinq\Symfony\Authenticator\Authenticator\CookieAuthenticator
#        - Fittinq\Symfony\Authenticator\Authenticator\HeaderAuthenticator
          
  access_control:
    - {path: '^/users', roles: ROLE_ADMIN_AUTHENTICATOR}


```
