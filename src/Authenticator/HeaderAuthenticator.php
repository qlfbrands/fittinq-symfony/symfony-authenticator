<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\Authenticator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Throwable;

class HeaderAuthenticator extends AbstractAuthenticator
{
    private UserProviderInterface $userProvider;

    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('Authorization');
    }

    public function authenticate(Request $request): Passport
    {
        try {
            $bearerToken = $request->headers->get('Authorization');
            $jwt = substr($bearerToken, strpos($bearerToken, "Bearer ") + 7);

            $badge = new UserBadge(
                $jwt,
                [$this->userProvider, 'loadUserByIdentifier']
            );

            /** We trigger this method just to ensure an authentication exception. Otherwise, the exception will only be
             * thrown after the AuthenticationSuccessEvent is dispatched.
             *
             * @see \Symfony\Component\Security\Http\Authentication\AuthenticatorManager::executeAuthenticator
             */
            $badge->getUser();

            return new SelfValidatingPassport($badge);
        } catch (Throwable $e) {
            throw new AuthenticationException();
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new Response("", Response::HTTP_UNAUTHORIZED);
    }
}